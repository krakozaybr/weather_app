package com.krak.weather;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void weatherIsCorrect(){
        String json = "{\n" +
                "    \"coord\": {\n" +
                "        \"lon\": 37.6173,\n" +
                "        \"lat\": 55.7558\n" +
                "    },\n" +
                "    \"weather\": [\n" +
                "        {\n" +
                "            \"id\": 804,\n" +
                "            \"main\": \"Clouds\",\n" +
                "            \"description\": \"пасмурно\",\n" +
                "            \"icon\": \"04n\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"base\": \"stations\",\n" +
                "    \"main\": {\n" +
                "        \"temp\": 281.9,\n" +
                "        \"feels_like\": 279.3,\n" +
                "        \"temp_min\": 281.08,\n" +
                "        \"temp_max\": 282.9,\n" +
                "        \"pressure\": 1023,\n" +
                "        \"humidity\": 80,\n" +
                "        \"sea_level\": 1023,\n" +
                "        \"grnd_level\": 1005\n" +
                "    },\n" +
                "    \"visibility\": 10000,\n" +
                "    \"wind\": {\n" +
                "        \"speed\": 4.7,\n" +
                "        \"deg\": 240,\n" +
                "        \"gust\": 12.03\n" +
                "    },\n" +
                "    \"clouds\": {\n" +
                "        \"all\": 100\n" +
                "    },\n" +
                "    \"dt\": 1635516250,\n" +
                "    \"sys\": {\n" +
                "        \"type\": 2,\n" +
                "        \"id\": 2018597,\n" +
                "        \"country\": \"RU\",\n" +
                "        \"sunrise\": 1635481750,\n" +
                "        \"sunset\": 1635515827\n" +
                "    },\n" +
                "    \"timezone\": 10800,\n" +
                "    \"id\": 524901,\n" +
                "    \"name\": \"Москва\",\n" +
                "    \"cod\": 200\n" +
                "}";
        WeatherData weatherData = WeatherData.parseJSON(json);
        assert weatherData != null;
        assertEquals(weatherData, WeatherData.parseJSON(weatherData.toJSON()));
    }
}