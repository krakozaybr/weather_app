package com.krak.weather;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;

public class GeolocationUpdaterService extends Service {
    public GeolocationUpdaterService() {
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        final String bestProvider = intent.getStringExtra(GeolocationManager.BEST_PROVIDER_PARAM);
        locationManager.requestLocationUpdates(
                bestProvider, 1000 * 100, 20, location -> {}
        );
        new Thread(){
            @Override
            public void run() {
                try{
                    Thread.sleep(1000);
                    stopSelf();
                } catch (InterruptedException ignored){}
            }
        }.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }
}