package com.krak.weather;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class WeatherData {
    private double latitude;
    private double longitude;
    private String weatherDescription;
    private double windSpeed;
    private int windDirection;
    private String country;
    private String cityName;

    private WeatherData() {
    }

    public static WeatherData parseJSON(String json) {
        WeatherData weatherData = new WeatherData();
        try {
            JSONObject data = new JSONObject(json);
            weatherData.latitude = data.getJSONObject("coord").getDouble("lat");
            weatherData.longitude = data.getJSONObject("coord").getDouble("lon");
            weatherData.weatherDescription = data.getJSONArray("weather").getJSONObject(0).getString("description");
            weatherData.windSpeed = data.getJSONObject("wind").getDouble("speed");
            weatherData.windDirection = data.getJSONObject("wind").getInt("deg");
            try {
                weatherData.cityName = data.getString("name");
            } catch (Throwable e){
                weatherData.cityName = "--";
            }
            try {
                weatherData.country = data.getJSONObject("sys").getString("country");
            } catch (Throwable e){
                weatherData.country = "--";
            }
            return weatherData;
        } catch (JSONException e){
            e.printStackTrace();
        } catch (NullPointerException e){
            Log.e("WEATHERDATA PARSER", "JSON STRING IS NULL");
        }
        return null;
    }

    public String toJSON(){
        try {
            JSONObject result = new JSONObject();
            JSONObject coord = new JSONObject();
            coord.put("lat", latitude);
            coord.put("lon", longitude);
            result.put("coord", coord);
            JSONArray weather = new JSONArray();
            JSONObject currentWeather = new JSONObject();
            currentWeather.put("description", weatherDescription);
            weather.put(currentWeather);
            result.put("weather", weather);
            JSONObject wind = new JSONObject();
            wind.put("speed", windSpeed);
            wind.put("deg", windDirection);
            result.put("wind", wind);
            result.put("name", cityName);
            JSONObject sys = new JSONObject();
            sys.put("country", country);
            result.put("sys", sys);
            return result.toString();
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherData that = (WeatherData) o;
        return Double.compare(that.getLatitude(), getLatitude()) == 0 &&
                Double.compare(that.getLongitude(), getLongitude()) == 0 &&
                Double.compare(that.getWindSpeed(), getWindSpeed()) == 0 &&
                getWindDirection() == that.getWindDirection() &&
                getWeatherDescription().equals(that.getWeatherDescription()) &&
                getCountry().equals(that.getCountry()) &&
                getCityName().equals(that.getCityName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLatitude(), getLongitude(), getWeatherDescription(), getWindSpeed(), getWindDirection(), getCountry(), getCityName());
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public int getWindDirection() {
        return windDirection;
    }

    public String getCountry() {
        return country;
    }

    public String getCityName() {
        return cityName;
    }
}
