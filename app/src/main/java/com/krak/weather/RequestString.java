package com.krak.weather;

import java.util.ArrayList;
import java.util.HashMap;

public class RequestString {
    private ArrayList<Parameter> params;
    private String address;

    public RequestString(String address, ArrayList<Parameter> params) {
        this.params = params;
        this.address = address;
    }

    public boolean addParameter(Parameter parameter){
        return params.add(parameter);
    }

    public boolean removeParameter(Parameter parameter){
        return params.remove(parameter);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(address);
        sb.append("?");
        for (Parameter param : params){
            sb.append(param.getName());
            sb.append("=");
            sb.append(param.getValue());
            sb.append("&");
        }
        sb.delete(sb.length() - 1, sb.length());
        return sb.toString();
    }

    static class Parameter{
        final String name;
        final String value;

        public Parameter(String name, Object value) {
            this.name = name;
            this.value = value.toString();
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
