package com.krak.weather;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.util.Arrays;

public class PermissionManager {

    private final Activity activity;
    private static final int requestCode = 123;
    private final String[] allPermissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private final String[] geolocationPermissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    public PermissionManager(Activity activity) {
        this.activity = activity;
    }

    public void requestPermission(String permission){
        if (!checkPermission(permission)) {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
        }
    }

    public void requestPermissions(String[] permissions){
        for (String permission : permissions){
            requestPermission(permission);
        }
    }

    public boolean checkPermission(String permission){
        if (ActivityCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED){
            Log.i("PERMISSION-CHECK", permission + " IS DENIED");
            return false;
        } else {
            Log.i("PERMISSION-CHECK", permission + " IS GRANTED");
            return true;
        }
    }

    public boolean checkPermissions(String[] permissions){
        boolean result = true;
        for (String permission : permissions){
            result = result && checkPermission(permission);
        }
        return result;
    }

    public String[] getGeolocationPermissions() {
        return Arrays.copyOf(geolocationPermissions, geolocationPermissions.length);
    }

    public String[] getAllPermissions() {
        return Arrays.copyOf(allPermissions, allPermissions.length);
    }
}
