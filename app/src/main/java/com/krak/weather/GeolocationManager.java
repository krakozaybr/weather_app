package com.krak.weather;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.List;

public class GeolocationManager {

    private Criteria criteria;
    private String bestProvider;
    private LocationManager locationManager;
    private double longitude;
    private double latitude;
    private Activity activity;

    public static final String BEST_PROVIDER_PARAM = "BEST_PROVIDER";

    public GeolocationManager(Activity activity){
        this.activity = activity;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);;
        setDefaultCriteria();
    }

    public double getLongitude(){
        return longitude;
    }

    public double getLatitude(){
        return latitude;
    }

    public void update(){
        bestProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider == null){
            bestProvider = LocationManager.GPS_PROVIDER;
        }
        Log.i("BEST-PROVIDER", bestProvider);
        @SuppressLint("MissingPermission")
        Location lastKnownLocation = getLastKnownLocation();
        latitude = lastKnownLocation.getLatitude();
        longitude = lastKnownLocation.getLongitude();
        updateGeolocation();
    }

    public void setDefaultCriteria(){
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
    }

    private void updateGeolocation() {
        Intent intent = new Intent(activity, GeolocationUpdaterService.class)
                .putExtra(BEST_PROVIDER_PARAM, bestProvider);
        activity.startService(intent);
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
        update();
    }

    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission")
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
