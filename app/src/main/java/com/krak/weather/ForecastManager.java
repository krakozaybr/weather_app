package com.krak.weather;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class ForecastManager {

    private final Activity activity;

    public static final String PARAM_TYPE = "TYPE";
    public static final String PARAM_LONGITUDE = "LONGITUDE";
    public static final String PARAM_LATITUDE = "LATITUDE";
    public static final String PARAM_CITY_ID = "PARAM_CITY_ID";
    public static final String PARAM_CITY_NAME = "PARAM_CITY_NAME";

    public static final String PARAM_JSON_RESULT = "JSON_RESULT";
    public static final String COORDS_TYPE_VALUE = "COORDS";
    public static final String CITY_ID_TYPE_VALUE = "CITY_ID";
    public static final String CITY_NAME_TYPE_VALUE = "CITY_NAME";

    private volatile WeatherData currentData = null;

    public ForecastManager(Activity activity) {
        this.activity = activity;
        Log.i("WEATHER UPDATE", "JSON IS RECEIVED");
    }

    public void updateWeatherDataByCoordinates(double latitude, double longitude){
        Log.i("WEATHER UPDATE", "UPDATE WEATHER DATA");
        Intent intent = new Intent(activity, RequestsService.class)
                .putExtra(PARAM_TYPE, COORDS_TYPE_VALUE)
                .putExtra(PARAM_LONGITUDE, longitude)
                .putExtra(PARAM_LATITUDE, latitude);
        activity.startService(intent);
    }

    public void updateWeatherDataByCityId(int cityId){
        Log.i("WEATHER UPDATE", "UPDATE WEATHER DATA");
        Intent intent = new Intent(activity, RequestsService.class)
                .putExtra(PARAM_TYPE, CITY_ID_TYPE_VALUE)
                .putExtra(PARAM_CITY_ID, cityId);
        activity.startService(intent);
    }

    public void updateWeatherDataByCityName(String cityName){
        Log.i("WEATHER UPDATE", "UPDATE WEATHER DATA");
        Intent intent = new Intent(activity, RequestsService.class)
                .putExtra(PARAM_TYPE, CITY_NAME_TYPE_VALUE)
                .putExtra(PARAM_CITY_NAME, cityName);
        activity.startService(intent);
    }

    public WeatherData getCurrentData() {
        return currentData;
    }

    public void setCurrentData(WeatherData currentData) {
        this.currentData = currentData;
    }
}
