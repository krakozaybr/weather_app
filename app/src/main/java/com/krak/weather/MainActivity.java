package com.krak.weather;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.krak.weather.databinding.ActivityMainBinding;

import java.util.Set;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String UPDATE_VIEWS = "VIEWS_UPDATER";

    private ActivityMainBinding binding;

    private PermissionManager permissionManager;
    private GeolocationManager geolocationManager;
    private ForecastManager forecastManager;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("INITIALIZATION", "START");
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initManagers();
        initReceivers();
        addListeners();
        registerReceivers();
        permissionManager.requestPermissions(permissionManager.getAllPermissions());
        if (permissionManager.checkPermissions(permissionManager.getAllPermissions())){
            geolocationManager.update();
        }
        updateInformation();
        loadSettings();
        updateInputViews();
        Log.i("INITIALIZATION", "END");
    }

    private void initManagers(){
        Log.i("INITIALIZATION", "INIT MANAGERS START");
        permissionManager = new PermissionManager(this);
        forecastManager = new ForecastManager(this);
        geolocationManager = new GeolocationManager(this);
        Log.i("INITIALIZATION", "INIT MANAGERS END");
    }

    private void initReceivers() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String json = intent.getStringExtra(ForecastManager.PARAM_JSON_RESULT);
                WeatherData currentWeatherData = WeatherData.parseJSON(json);
                forecastManager.setCurrentData(currentWeatherData);
                updateView(forecastManager.getCurrentData());
            }
        };
    }

    private void addListeners(){
        binding.updateBtn.setOnClickListener(v -> updateInformation());
        binding.changeInputTypeBtn.setOnClickListener(v -> showInputTypeChooseDialog());
    }

    private void registerReceivers() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(UPDATE_VIEWS));
    }

    private void updateWeatherData() {
        double longitude = 0, latitude = 0;
        String input1String = binding.input1.getText().toString().trim();
        String input2String = binding.input2.getText().toString().trim();
        switch(Settings.getCurrentInputType()){
            case CURRENT_LOCATION:
                latitude = geolocationManager.getLatitude();
                longitude = geolocationManager.getLongitude();
                forecastManager.updateWeatherDataByCoordinates(latitude, longitude);
                break;
            case COORDINATES:
                try {
                    latitude = Double.parseDouble(input1String);
                    longitude = Double.parseDouble(input2String);
                } catch (NumberFormatException e){
                    Toast.makeText(this, "Incorrect input", Toast.LENGTH_SHORT).show();
                    Log.i("INPUT", "INCORRECT INPUT");
                }
                forecastManager.updateWeatherDataByCoordinates(latitude, longitude);
                break;
            case CITY_ID:
                int cityId = 0;
                try {
                    cityId = Integer.parseInt(input1String);
                } catch (NumberFormatException e){
                    Toast.makeText(this, "Incorrect input", Toast.LENGTH_SHORT).show();
                    Log.i("INPUT", "INCORRECT INPUT");
                }
                forecastManager.updateWeatherDataByCityId(cityId);
                break;
            case CITY_NAME:
                forecastManager.updateWeatherDataByCityName(input1String);
        }
        binding.input1.setText("");
        binding.input2.setText("");
    }

    private void updateInformation(){
        if (!permissionManager.checkPermission(Manifest.permission.INTERNET)){
            permissionManager.requestPermission(Manifest.permission.INTERNET);
            return;
        }
        if (!permissionManager.checkPermissions(permissionManager.getGeolocationPermissions())
                && Settings.getCurrentInputType() == Settings.INPUT_TYPE.CURRENT_LOCATION) {
            permissionManager.requestPermissions(permissionManager.getGeolocationPermissions());
            return;
        }
        Log.i("WEATHER UPDATE", "STARTING GETTING JSON");
        updateWeatherData();
        try {
            Thread.sleep(100);
        } catch (InterruptedException ignored){}
        updateView(forecastManager.getCurrentData());
    }

    private void loadSettings() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        Settings.setCurrentInputType(Settings.INPUT_TYPE.valueOf(
                preferences.getString("INPUT", Settings.DEFAULT_INPUT_TYPE.toString())));
        Settings.updateTranslations(getResources());
    }

    private void saveSettings(){
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = preferences.edit();
        ed.putString("INPUT", Settings.getCurrentInputType().toString());
        ed.commit();
    }

    private void updateInputViews(){
        switch(Settings.getCurrentInputType()){
            case CURRENT_LOCATION:
                binding.input1.setVisibility(View.GONE);
                binding.input2.setVisibility(View.GONE);
                Settings.setCurrentInputType(Settings.INPUT_TYPE.CURRENT_LOCATION);
                break;
            case COORDINATES:
                binding.input1.setVisibility(View.VISIBLE);
                binding.input2.setVisibility(View.VISIBLE);
                binding.input1.setHint(getResources().getString(R.string.latitude));
                binding.input2.setHint(getResources().getString(R.string.longitude));
                Settings.setCurrentInputType(Settings.INPUT_TYPE.COORDINATES);
                break;
            case CITY_ID:
                binding.input1.setVisibility(View.VISIBLE);
                binding.input2.setVisibility(View.GONE);
                binding.input1.setHint(getResources().getString(R.string.cityIDInputType));
                Settings.setCurrentInputType(Settings.INPUT_TYPE.CITY_ID);
                break;
            case CITY_NAME:
                binding.input1.setVisibility(View.VISIBLE);
                binding.input2.setVisibility(View.GONE);
                binding.input1.setHint(getResources().getString(R.string.cityNameInputType));
                Settings.setCurrentInputType(Settings.INPUT_TYPE.CITY_NAME);
        }
        binding.input1.setText("");
        binding.input2.setText("");
    }

    private void updateView(WeatherData currentData){
        Log.i("WEATHER UPDATE", "END");
        if (currentData == null){
            Log.e("VIEW-UPDATING", "THE WEATHERDATA IS NULL");
            return;
        }
        binding.tvLongitude.setText(getResources().getString(R.string.longitude) + ": " + currentData.getLongitude());
        binding.tvLatitude.setText(getResources().getString(R.string.latitude) + ": " + currentData.getLatitude());
        binding.tvWindSpeed.setText(getResources().getString(R.string.windSpeed) + " " + currentData.getWindSpeed());
        binding.tvWindDirection.setText(getResources().getString(R.string.windDirection) + currentData.getWindDirection());
        binding.tvWeatherDescription.setText(getResources().getString(R.string.weatherDescription) + " " + currentData.getWeatherDescription());
        binding.tvCountry.setText(getResources().getString(R.string.country) + " " + currentData.getCountry());
        binding.tvCityName.setText(getResources().getString(R.string.cityName) + " " + currentData.getCityName());
    }

    private void showInputTypeChooseDialog(){
        final String [] alternatives = new String[Settings.INPUT_TYPE.values().length];
        final String okString = getResources().getString(R.string.ok);
        final String selectedString = getResources().getString(R.string.selected);
        final String chooseInputTypeString = getResources().getString(R.string.chooseInputType);
        final String cancelString = getResources().getString(R.string.cancel);
        final int[] selected = {Settings.getCurrentInputType().ordinal()};

        for (int i = 0; i < alternatives.length; i++){
            alternatives[i] = Settings.getTranslations().get(Settings.INPUT_TYPE.values()[i].toString());
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(chooseInputTypeString)
                .setSingleChoiceItems(alternatives, selected[0],
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                Toast.makeText(
                                        MainActivity.this,
                                        selectedString + ": " + alternatives[item],
                                        Toast.LENGTH_SHORT
                                ).show();
                                selected[0] = item;
                            }
                        }
                )
                .setPositiveButton(okString, (dialog, id) -> {
                    String currentMode = alternatives[selected[0]];
                    Settings.setCurrentInputType(Settings.INPUT_TYPE.values()[selected[0]]);
                    updateInputViews();
                    Log.i("INPUT_TYPE_DIALOG", currentMode + " CHOSEN");
                })
                .setNegativeButton(cancelString, (dialog, id) -> {
                    Log.i("INPUT_TYPE_DIALOG", "CANCELLED");
                });
        Log.i("INPUT_TYPE_DIALOG", "MESSAGE IS SENT");
        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }
}