package com.krak.weather;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RequestsService extends Service {

    private static final String APPID = "ac33291035975639384467c8e60e55ae";
    private static final String ADRESS = "http://api.openweathermap.org/data/2.5/weather";
    private ExecutorService executorService;

    public RequestsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        executorService = Executors.newFixedThreadPool(10);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String type = intent.getStringExtra(ForecastManager.PARAM_TYPE);
        RequestString request = null;
        if (type.equals(ForecastManager.COORDS_TYPE_VALUE)){
            request = requestByCoords(intent);
        } else if (type.equals(ForecastManager.CITY_ID_TYPE_VALUE)){
            request = requestByCityId(intent);
        } else if (type.equals(ForecastManager.CITY_NAME_TYPE_VALUE)){
            request = requestByCityName(intent);
        }
        if (request != null) {
            Log.i("REQUEST", request.toString());
            request.addParameter(new RequestString.Parameter("appid", APPID));
            request.addParameter(new RequestString.Parameter("lang", "RU"));
            Requester requester = new Requester(request.toString());
            executorService.execute(requester);
            Log.i("WEATHER UPDATE", "THREAD IS LAUNCHED");
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new Binder();
    }

    private RequestString requestByCoords(final Intent intent){
        final double longitude = intent.getDoubleExtra(ForecastManager.PARAM_LONGITUDE, 0);
        final double latitude = intent.getDoubleExtra(ForecastManager.PARAM_LATITUDE, 0);
        final ArrayList<RequestString.Parameter> params = new ArrayList<>();
        params.add(new RequestString.Parameter("lat",
                String.format("%.4f", latitude).replace(",", ".")));
        params.add(new RequestString.Parameter("lon",
                String.format("%.4f", longitude).replace(",", ".")));
        return new RequestString(ADRESS, params);
    }

    private RequestString requestByCityId(Intent intent){
        final int cityId = intent.getIntExtra(ForecastManager.PARAM_CITY_ID, 0);
        final ArrayList<RequestString.Parameter> params = new ArrayList<>();
        params.add(new RequestString.Parameter("id", cityId));
        return new RequestString(ADRESS, params);
    }

    private RequestString requestByCityName(Intent intent){
        final String cityName = intent.getStringExtra(ForecastManager.PARAM_CITY_NAME);
        final ArrayList<RequestString.Parameter> params = new ArrayList<>();
        params.add(new RequestString.Parameter("q", cityName));
        return new RequestString(ADRESS, params);
    }

    class Requester implements Runnable{
        String request;

        public Requester(String request) {
            this.request = request;
        }

        @Override
        public void run() {
            Log.i("WEATHER UPDATE", "GETTING JSON");
            String result = sendRequest(request);
            Intent intent = new Intent(MainActivity.UPDATE_VIEWS)
                    .putExtra(ForecastManager.PARAM_JSON_RESULT, result);
            LocalBroadcastManager.getInstance(RequestsService.this).sendBroadcast(intent);
            Log.i("WEATHER UPDATE", "JSON IS SENT");
            stopSelf();
        }

        private String sendRequest(String request){
            BufferedReader reader = null;
            InputStream stream = null;
            HttpURLConnection connection = null;
            try{
                final URL url = new URL(request);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.connect();
                stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) reader.close();
                    if (stream != null) stream.close();
                    if (connection != null) connection.disconnect();
                } catch (IOException e){}
            }
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}