package com.krak.weather;

import android.content.res.Resources;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Settings {
    public static enum INPUT_TYPE {
        CURRENT_LOCATION,
        COORDINATES,
        CITY_ID,
        CITY_NAME
    }

    private static final HashMap<String, String> translations = new HashMap<>();
    public static final INPUT_TYPE DEFAULT_INPUT_TYPE = INPUT_TYPE.CURRENT_LOCATION;

    private static INPUT_TYPE currentInputType = DEFAULT_INPUT_TYPE;

    public static INPUT_TYPE getCurrentInputType() {
        return currentInputType;
    }

    public static void setCurrentInputType(INPUT_TYPE currentInputType) {
        Settings.currentInputType = currentInputType;
    }

    public static void updateTranslations(Resources resources){
        translations.put(INPUT_TYPE.CITY_ID.toString(), resources.getString(R.string.cityIDInputType));
        translations.put(INPUT_TYPE.CITY_NAME.toString(), resources.getString(R.string.cityNameInputType));
        translations.put(INPUT_TYPE.COORDINATES.toString(), resources.getString(R.string.coordinatesInputType));
        translations.put(INPUT_TYPE.CURRENT_LOCATION.toString(), resources.getString(R.string.currentLocationInputType));
    }

    public static Map<String, String> getTranslations() {
        return Collections.unmodifiableMap(translations);
    }
}
